function add(num1, num2) {
    return num1 + num2;
}

var age = 20;

const player1 = {
    name: "Mario",
    score: 2500
};

const player2 = {
    name: "Luigi",
    score: 2500
};

console.log(player1.score === player2.score);

const car = { brand: "Toyota", color: "green" };

if (car.color === "red") {
    console.log("Cool car!");
} else if (car.color === "green") {
    console.log("Nice car!");
} else if (car.color === "yellow") {
    console.log("Meh!");
} else {
    console.log("Nah!");
}
