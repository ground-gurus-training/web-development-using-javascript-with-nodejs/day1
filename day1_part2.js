// Declarative
// Functional
// Object-Oriented

const cars = ["BMW", "Volvo", "Saab", "Ford", "Fiat", "Audi"];

// 1st part, initialization
// 2nd part, condition
// 3rd part, incrementation
// tranditional for loop
for (let i = 0; i < cars.length; i++) {

    if (i % 2 === 0) {
       console.log(cars[i]);
    }

}

console.log("--------");

// traditional for loop
for (let i = cars.length - 1; i >= 0; i--) {
  console.log(cars[i]); // print 5th element first
}

console.log("--------");

// enhanced for loop/ for each
for (let car of cars) {
    console.log(car);
}

console.log("--------");

// Functional approach
cars.forEach(function(car) {
  console.log(car);
});
